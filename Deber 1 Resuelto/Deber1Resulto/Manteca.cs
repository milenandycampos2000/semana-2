﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deber1Resulto
{

    /*
     
    3. En las Manteca el mensaje debe ser el siguiente.
    "La Manteca XX tiene X cantidad de Vitaminas ademas esta compuesta de X Lípidos y X Proteinas
     
     */
    class Manteca: Producto
    {

        //Atributo
        private double lipidos;

        // Metodos 

        public override string obtenerDescripcionProducto()
        {
            return "La manteca: " + Nombre + " tiene " + Vitamina + " g de vitaminas además esta compuesta de: " + lipidos + " g de lipidos y " 
                + Proteina + " g proteina";
        }

        // gets and sets

        public double Lipidos
        {
            get { return lipidos; }
            set { lipidos = value; }
        }
    }
}
